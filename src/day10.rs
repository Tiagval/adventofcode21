fn get_score(c: char, correction: &str) -> u128{
    let map = if correction == "corruption" {
        std::collections::HashMap::from([
        (')', 3),
        (']', 57),
        ('}', 1197),
        ('>', 25137),
    ])}
    else{
      std::collections::HashMap::from([
        ('(', 1),
        ('[', 2),
        ('{', 3),
        ('<', 4),
    ])
    };
    map[&c]
}

fn get_index(list: &[char], c: char) -> Option<usize>{
    list.iter().position(|&o| o == c)
}

fn read_line(line: &[char]) -> (u128, &str){
    let opening = ['(', '[', '{', '<'];
    let closing = [')', ']', '}', '>'];
    let mut process: Vec<char> = Vec::new();

    for c in line{
        let c_type: &str = if opening.contains(c) {"opening"} else {"closing"};
        if c_type == "opening"{
            process.push(*c);
        }
        if let Some(index) = get_index(&closing, *c){
            if *process.last().unwrap() == opening[index]{
                process.pop();
            }
            else{
                return (get_score(*c, "corruption"), "corruption");
            }
        }
    }
    if !process.is_empty(){
        process.reverse();
        let remaining = process.clone();
        //Remove complete chunks
        for c in remaining{
            if closing.contains(&c){
                process.remove(0);
                process.remove(0);

            }
        }
        let mut total = 0;
        for c in process{
            let score = get_score(c, "completion");
            total = total*5 + score;
        }
        return (total, "completion");
    }
    (0,"A")
}

fn get_lines() -> Vec<Vec<char>>{
    let data = std::fs::read_to_string("./inputs/day10.txt").unwrap();
    let lines: Vec<&str> = data.lines().collect();
    let mut matrix: Vec<Vec<char>> = Vec::new();
    for line in lines{
        let l: Vec<char> = line.chars().collect();
        matrix.push(l);
    }
    matrix
}

fn get_total_score(correction: &str) -> u128{
    let matrix = get_lines();
    let mut total: Vec<u128> = Vec::new();
    for line in matrix{
        let out = read_line(&line);
        if out.1 == correction{ total.push(out.0)}
    }
    if correction == "corruption"{
        return total.iter().sum();
    }
    else{
        total.sort_unstable();
        total[total.len()/2]
    }
}
pub fn solve_part_1() ->u128{
     get_total_score("corruption")
}

pub fn solve_part_2() -> u128{
    get_total_score("completion")

}
