#[cfg(test)]
mod day1;
#[cfg(test)]
mod day2;
#[cfg(test)]
mod day3;
#[cfg(test)]
mod day4;
#[cfg(test)]
mod day5;
#[cfg(test)]
mod day6;
#[cfg(test)]
mod day7;
#[cfg(test)]
mod day8;
#[cfg(test)]
mod day9;
#[cfg(test)]
mod day10;
#[cfg(test)]
mod day11;
#[cfg(test)]
mod day12;

#[cfg(test)]
mod tests {
    use crate::day1;
    use crate::day2;
    use crate::day3;
    use crate::day4;
    use crate::day5;
    use crate::day6;
    use crate::day7;
    use crate::day8;
    use crate::day9;
    use crate::day10;
    use crate::day11;
    use crate::day12;

    //#[cfg(test)]
    #[test]
    fn day1(){
        let answer = day1::solve_part_1();
        println!("{}",answer);
        assert_eq!(answer,1266);

        let answer = day1::solve_part_2();
        println!("{}",answer);
        assert_eq!(answer,1217);
    }

    #[test]
    fn day2(){
        let answer = day2::solve_part_1();
        println!("{}",answer);
        assert_eq!(answer,1840243);

        let answer = day2::solve_part_2();
        println!("{}",answer);
        assert_eq!(answer,1727785422);
    }

    #[test]
    fn day3(){
        let answer = day3::solve_part_1();
        println!("{}",answer);
        assert_eq!(answer,749376);

        let answer = day3::solve_part_2();
        println!("{}",answer);
        assert_eq!(answer,2372923);
    }

    #[test]
    fn day4(){
        let answer = day4::solve_part_1();
        println!("{}", answer);
        assert_eq!(answer,14093);

        let answer = day4::solve_part_2();
        println!("{}", answer);
        assert_eq!(answer,17388);
    }

    #[test]
    fn day5(){
        let answer = day5::solve_part_1();
        println!("{}", answer);
        assert_eq!(answer,5294);

        let answer = day5::solve_part_2();
        println!("{}", answer);
        assert_eq!(answer,21698);
    }
    #[test]
    fn day6(){
        let answer = day6::solve_part_1();
        println!("{}", answer);
        assert_eq!(answer,375482);

        let answer = day6::solve_part_2();
        println!("{}", answer);
        assert_eq!(answer,1689540415957);
    }
    #[test]
    fn day7(){
        let answer = day7::solve_part_1();
        println!("{}", answer);
        assert_eq!(answer, 351901);

        let answer = day7::solve_part_2();
        println!("{}", answer);
        assert_eq!(answer,101079875);
    }
    #[test]
    fn day8(){
        let answer = day8::solve_part_1();
        println!("{}", answer);
        assert_eq!(answer, 381);

        let answer = day8::solve_part_2();
        println!("{}", answer);
        assert_eq!(answer,1023686);
    }
    #[test]
    fn day9(){
        let answer = day9::solve_part_1();
        println!("{}", answer);
        assert_eq!(answer, 575);

        let answer = day9::solve_part_2();
        println!("{}", answer);
        assert_eq!(answer,1019700);
    }
    #[test]
    fn day10(){
        let answer = day10::solve_part_1();
        println!("{}", answer);
        assert_eq!(answer,318081);

        let answer = day10::solve_part_2();
        println!("{}", answer);
        assert_eq!(answer,4361305341);
    }
    #[test]
    fn day11(){
        let answer = day11::solve_part_1();
        println!("{}", answer);
        assert_eq!(answer,1729);

        let answer = day11::solve_part_2();
        println!("{}", answer);
        assert_eq!(answer,237);
    }
        #[test]
    fn day12(){
        let answer = day12::solve_part_1();
        println!("{}", answer);
        assert_eq!(answer,1);

        let answer = day12::solve_part_2();
        println!("{}", answer);
        assert_eq!(answer,1);
    }
}

fn main() {
    println!("Have a good day :)");
}
