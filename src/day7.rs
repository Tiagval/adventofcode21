fn median(nums: &[i32]) ->i32{
    let mut arr = nums.to_owned();
    arr.sort_unstable();
    if arr.len() % 2 == 0{
        let left_i = arr.len()/2 -1;
        let right_i = arr.len()/2;
        let left = arr[left_i];
        let right = arr[right_i];
        (left + right)/2
    }
    else{
        arr[(arr.len()/2)]
    }
}

fn triangle_sum(n: i32) -> i32{
    (n*(n+1))/2
}

fn calculate_cost(arr: &[i32], pos: i32, linear: bool) -> i32{
    let mut cost = 0;
    for i in arr{
        let diff = (*i as i32 - pos).abs();
        if linear {
            cost += (diff).abs()
        }
        else{
            cost += triangle_sum(diff);
        }
    }
    cost
}

fn average(arr: &[i32]) -> i32{
    let sum:i32 = arr.iter().sum();
    sum/(arr.len() as i32)
}

pub fn solve_part_1() -> i32 {
    let data = std::fs::read_to_string("./inputs/day7.txt").unwrap();
    let crabs: Vec<i32>= data.trim().split(',').map(|s| s.parse().expect("parse")).collect();
    calculate_cost(&crabs, median(&crabs), true)
}

pub fn solve_part_2() -> i32 {
    let data = std::fs::read_to_string("./inputs/day7.txt").unwrap();
    let crabs: Vec<i32> = data.trim().split(',').map(|s| s.parse().expect("parse")).collect();
    let avg = average(&crabs) as i32;
    
    calculate_cost(&crabs,avg, false)
}
