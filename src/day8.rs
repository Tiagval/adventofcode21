use itertools::Itertools;

#[derive(Debug)]
struct Digits{
    zero: String,
    one: String,
    two: String,
    three: String,
    four: String,
    five: String,
    six: String,
    seven: String,
    eight: String,
    nine: String,
}

impl Digits {
    fn new(entry: &str) -> Digits{
        let mut digits = Digits
        {
            zero: "!".to_string(),
            one: "!".to_string(),
            two: "!".to_string(),
            three: "!".to_string(),
            four: "!".to_string(),
            five: "!".to_string(),
            six: "!".to_string(),
            seven: "!".to_string(),
            eight: "!".to_string(),
            nine: "!".to_string()};
        digits.set_unique_digits(entry);
        digits.set_dim6_digits(entry);
        digits.set_dim5_digits(entry);
        digits
    }

    fn set_unique_digits(&mut self, entry: &str){
        for digit in entry.split_whitespace(){
            if digit.len() == 2 {self.one = sort_string(digit).clone()} //1
            if digit.len() == 4 {self.four = sort_string(digit).clone()} //4
            if digit.len() == 3 {self.seven = sort_string(digit).clone()} //7
            if digit.len() == 7 {self.eight = sort_string(digit).clone()} //8
        }
    }

    fn set_dim6_digits(&mut self, entry: &str){
        for digit in entry.split_whitespace(){
            if digit.len() == 6 {
                //Checking for 6
                let mut matches_one = true;
                for ch in self.one.chars(){ // 1 does not overlap with 6
                    if !digit.contains(ch){
                        matches_one = false;
                    }
                }
                if !matches_one{self.six = sort_string(digit);}
                //Checking for 9/0
                else{
                    let mut matches_four = true;
                    for ch in self.four.chars(){ //4 overlaps with 9
                        if !digit.contains(ch) {
                            matches_four = false;
                        }
                    }
                    if matches_four{self.nine = sort_string(digit);}
                    else{self.zero = sort_string(digit);}
                }
            }
        }
    }

    fn set_dim5_digits(&mut self, entry: &str){
        for digit in entry.split_whitespace(){
            if digit.len() == 5{
                let mut overlaps = true;
                for ch in self.one.chars(){
                    if !digit.contains(ch){
                        overlaps = false;
                    }
                }
                //3 overlaps with 1, 2 and 5 do not
                if overlaps{self.three = sort_string(digit)}
                else{
                    let mut mismatches = 0;
                    //5 overlaps with 6, 2 does not
                    for ch in digit.chars(){
                        if !self.six.contains(ch){mismatches += 1}
                    }
                    if mismatches == 0 {
                        self.five = sort_string(digit);
                    }
                    else if mismatches == 1{
                        self.two = sort_string(digit);
                    }
                }
            }
        }
    }

    fn get_output(&self, to_find: &str) -> u32{
        let to_loop = [
            self.zero.clone(), self.one.clone(), self.two.clone(), self.three.clone(), self.four.clone(),
            self.five.clone(), self.six.clone(), self.seven.clone(), self.eight.clone(), self.nine.clone(),
        ];
        let mut total_out = String::new();
        for output in to_find.split_whitespace(){
            let out =  sort_string(output).to_string();
            let index = to_loop.to_vec().iter().position(|s| s == &out).unwrap();
            total_out += &index.to_string();

        }
        total_out.parse().unwrap()
    }
}

fn sort_string(s: &str) -> String{
    s.chars().sorted().collect::<String>()
}

pub fn solve_part_1() -> u32{
    let data = std::fs::read_to_string("./inputs/day8.txt").unwrap();
    let entries: Vec<(&str,&str)> = data
        .lines()
        .filter_map(|l| {
            l.split(" | ")
             .collect_tuple()
        })
        .collect();
    let mut total = 0;
    for entry in &entries{
        for output in entry.1.split_whitespace(){
            if output.len() != 5 && output.len() != 6{
                total += 1;
            };
        }
    }
    total
}

pub fn solve_part_2() -> u32{
    let data = std::fs::read_to_string("./inputs/day8.txt").unwrap();
    let entries: Vec<(&str,&str)> = data
        .lines()
        .filter_map(|l| {
            l.split(" | ")
             .collect_tuple()
        })
        .collect();
    let mut total = 0;
    for entry in &entries{
        let digits = &Digits::new(entry.0);
        total += digits.get_output(entry.1);
    }
    total
}
