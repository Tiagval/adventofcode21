use std::{fs, io};

#[derive(Debug)]
struct Board{
    rows: Vec<[i32;5]>,
    winner: bool,
    score: f32,
    matches: Vec<i32>,
}

impl Board {
    pub fn new(board : &[String]) -> Board{
        Board{
            rows : Board::make_rows(board),
            winner : false,
            score : 0.0,
            matches : Vec::new(),
        }
    }

    pub fn copy(board: &Board) -> Board{
        let board_rows = board.rows.clone();
        let is_winner = board.winner;
        let board_score = board.score;
        let board_matches = board.matches.clone();

        Board{rows: board_rows, winner: is_winner, score: board_score, matches: board_matches}
    }

    fn make_rows(board: &[String]) -> Vec<[i32;5]>{
        let mut rows: Vec<[i32;5]> = Vec::new();
        let mut cols: Vec<[i32;5]> = Vec::new();
        //Making empty arrays
        for _ in 0..5{
            let tmp_arr = [0;5];
            cols.push(tmp_arr);
        }
        for (i, row) in board.iter().enumerate(){
            let parsed = row.split_whitespace().collect::<Vec<&str>>();
            let mut tmp_row = [0;5];
            for (j, element) in parsed.iter().enumerate(){
                let fixed_element = element.parse::<i32>().unwrap();
                tmp_row[j] = fixed_element;
                cols[j][i] = fixed_element;
            }
            rows.push(tmp_row);
        }
        rows.extend(cols);
        rows
    }

    fn add(&mut self, no: i32){
        if !self.matches.contains(&no){
            self.matches.push(no);
        }
        if self.matches.len() >= 5{
            self.check_win(no);
        }
    }

    fn check_win(&mut self, no: i32){
        let matches = self.matches.clone();
        let rows = self.rows.clone();
        for row in &rows{
            let mut counter = 0;
            for number in row{
                if !matches.contains(number){
                    continue;
                }
                else{
                    counter+=1;
                }
                if counter == 5{
                    self.winner = true;
                    self.set_score(no);
                }
            }
        }
    }

    fn set_score(&mut self, number: i32){
        let matches = self.matches.clone();
        let mut score: f32 = 0.0;
        let rows = self.rows.clone();
        for row in rows{
            for no in row{
                let index = matches.contains(&no);
                if !index{
                    score += no as f32;
                }
            }
        }
        self.score = (score/2.0)*(number as f32);
    }
}

fn get_chosen_numbers(data: &str) -> String{
    let lines = data.lines();
    let mut numbers = String::new();
    for (counter, line) in lines.enumerate(){
        if counter < 1{numbers.push_str(line)};
    }
    numbers
}

fn parse_boards(data: &str) -> Vec<Vec<String>>{
    let lines = data.lines().skip(1);
    let mut boards = Vec::new();
    let mut rows = Vec::new();
    for line in lines{
        if !line.is_empty(){
            rows.push(String::from(line));
        }
        if rows.len() == 5{
            boards.push(rows.clone());
            rows.clear();
        }
    }
    boards
}

fn make_board_list(boards: Vec<Vec<String>>) -> Vec<Board>{
    let mut board_list: Vec<Board> = Vec::new();
    for board in boards{
        board_list.push(Board::new(&board));
    }
    board_list
}

fn play_bingo(numbers: &str, board_list: &mut Vec<Board>) -> Result<(Board, usize), io::Error>{
    for number in numbers.split(','){
        let no = number.parse::<i32>().unwrap();
        for (i, board) in board_list.iter_mut().enumerate(){
            if board.winner{
                continue;
            }
            let rows = board.rows.clone();
            for row in rows{
                let index = row.contains(&no);
                if index{
                    board.add(no);
                }
                if board.winner{
                    let winner_board = Board::copy(&board_list[i]);
                    return Ok((winner_board, i));
                }
            }
        }
    }
    panic!("Now winner was found")
}

pub fn solve_part_1() -> i32{
    let data = fs::read_to_string("./inputs/day4.txt").unwrap();
    let numbers = get_chosen_numbers(&data);
    let boards = parse_boards(&data);
    let mut board_list = make_board_list(boards);
    match play_bingo(&numbers, &mut board_list){
        Ok(tuple) => tuple.0.score as i32,
        Err(_) => panic!("No winner found"),
    }
}

pub fn solve_part_2() -> i32{
    let data = fs::read_to_string("./inputs/day4.txt").unwrap();
    let numbers = get_chosen_numbers(&data);
    let boards = parse_boards(&data);
    let mut board_list = make_board_list(boards);
    let no_boards = &board_list.len();
    let mut winner_list = Vec::new();
    while winner_list.len() != *no_boards {
        match play_bingo(&numbers,&mut board_list){
            Ok(tuple) => winner_list.push(tuple.1),
            Err(_) => panic!("No winner found"),
        };
    }
    board_list[winner_list[winner_list.len() -1]].score as i32
}
