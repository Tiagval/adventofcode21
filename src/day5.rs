use itertools::Itertools;

#[derive(Debug)]
struct Line{
    x0: u32,
    x1: u32,
    y0: u32,
    y1: u32,
}

impl Line {
    pub fn new(line: (u32,u32,u32,u32)) -> Line{
        Line { x0: line.0, x1: line.2, y0: line.1, y1: line.3}
    }

    fn intersects(&self) -> Vec<(u32,u32)>{
        let mut inter_list = Vec::new();
        let mut iter_x = self.x0 as i32;
        let mut iter_y = self.y0 as i32;

        //Outputs +1 if x1 > x0, 0 if equal, -1 otherwise
        let dx = (self.x1 as i32 - self.x0 as i32).signum();
        let dy = (self.y1 as i32 - self.y0 as i32).signum();
        while (iter_x, iter_y) != (self.x1 as i32 + dx, self.y1 as i32 + dy){
            inter_list.push((iter_x as u32,iter_y as u32));
                iter_x += dx;
                iter_y += dy;
            // inter_list.push((iter_x,iter_y));
        }
        inter_list
    }
}

fn get_lines(data: &str) -> Result<Vec<(u32,u32,u32,u32)>, std::string::ParseError>{
    let lines: Vec<(u32,u32,u32,u32)> = data
      .lines()
      .filter_map(|l| {
        l.split(" -> ")
          .map(|s| s.split(','))
          .flatten()
          .map(|i| i.parse().unwrap())
          .collect_tuple()
      })
      .collect();
    Ok(lines)
}

fn make_lines_list(lines: Vec<(u32,u32,u32,u32)>, diag: bool)->Result<Vec<Line>, std::string::ParseError>{
    let mut line_list = Vec::new();
    for line in lines{
        if line.0 == line.2 || line.1 == line.3 || diag{
            line_list.push(Line::new(line));
        }
    }
    Ok(line_list)
}

fn count_intersections(line_list: Vec<Line>) -> std::collections::HashMap<(u32,u32), u32>{
    let mut intersection_count = std::collections::HashMap::new();
    for line in line_list{
        let inter = line.intersects();
        for i in inter{
            if intersection_count.contains_key(&i){
                *intersection_count.get_mut(&i).unwrap() += 1;
                continue;
            }
            intersection_count.insert(i, 1);
        }
    }
    intersection_count
}

pub fn solve_part_1()->u32{
    let data = std::fs::read_to_string("./inputs/day5.txt").unwrap();
    let lines = match get_lines(&data){
        Ok(lines) => lines,
        Err(_) => panic!("Couldn't read lines!"),
    };
    let line_list = make_lines_list(lines,false).unwrap();
    let inter_count = count_intersections(line_list);
    inter_count.iter().filter(|i| *i.1 > 1).count() as u32
}

pub fn solve_part_2()->u32{
    let data = std::fs::read_to_string("./inputs/day5.txt").unwrap();
    let lines = match get_lines(&data){
        Ok(lines) => lines,
        Err(_) => panic!("Couldn't read lines!"),
    };
    let line_list = make_lines_list(lines,true).unwrap();
    let inter_count = count_intersections(line_list);
    inter_count.iter().filter(|i| *i.1 > 1).count() as u32
}
