fn next_day(mut next_state: std::collections::HashMap<u128,u128>) -> std::collections::HashMap<u128,u128>{
    let zeros = *next_state.get(&0).unwrap();
    for i in 1..9{
        *next_state.get_mut(&(i-1)).unwrap() = *next_state.get(&(i)).unwrap();
    }
    *next_state.get_mut(&6).unwrap() += zeros;
    *next_state.get_mut(&8).unwrap() = zeros;
    next_state
}

fn get_initial_state(initial_state: Vec<u128>) -> std::collections::HashMap<u128,u128>{
    let mut map = std::collections::HashMap::new();
    for i in initial_state{
        if let std::collections::hash_map::Entry::Vacant(e) = map.entry(i) {
            e.insert(1);
        } else {
            *map.get_mut(&i).unwrap() += 1;
        }
    }
    for i in 0..9{
        if let std::collections::hash_map::Entry::Vacant(e) = map.entry(i) {
            e.insert(0);
        } else {
            continue;
        }
    }
    map
}

pub fn solve_part_1() -> u128{
    let data = std::fs::read_to_string("./inputs/day6.txt").unwrap();
    let initial_state: Vec<u128> = data.trim().split(',').map(|s| s.parse().expect("parse")).collect();
    let ndays = 80;
    let mut next_state = get_initial_state(initial_state);
    for _ in 0..ndays{
        next_state = next_day(next_state);
    }
    next_state.values().sum()
}

pub fn solve_part_2() -> u128{
    let data = std::fs::read_to_string("./inputs/day6.txt").unwrap();
    let initial_state: Vec<u128> = data.trim().split(',').map(|s| s.parse().expect("parse")).collect();
    let ndays = 256;
    let mut next_state = get_initial_state(initial_state);
    for _ in 0..ndays{
        next_state = next_day(next_state);
    }
    next_state.values().sum()
}
