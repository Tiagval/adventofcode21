use std::fs;

fn get_data(filename: &str) -> Result<Vec<i32>, ()>{
    let depth_list : Vec<i32> = fs::read_to_string(filename).expect("Could no read file")
    .trim().lines().map(|x| x.parse().expect("parse")).collect();
    Ok(depth_list)
}
pub fn solve_part_1() -> i16{
    let mut total: i16 = 0; //counter variable
    let mut last: i32 = 999999999; //dumb starting number to offset the first comparison
    let filename: &str = "./inputs/day1.txt";

    let depth_list = get_data(filename).unwrap();

    //Solving part a
    for depth in &depth_list{
        if depth > &last {
            total += 1;
        }
        last = *depth;
    }
    total
}

pub fn solve_part_2() -> i16{
    let mut total: i16 = 0; //counter variable
    let mut last: i32 = 999999999; //dumb starting number to offset the first comparison

    let mut iter: usize= 0;

    let filename: &str = "./inputs/day1.txt";

    let depth_list = get_data(filename).unwrap();


    while iter < &depth_list.len() -2 {
        let slice: i32 = depth_list[iter] + depth_list[iter + 1] + depth_list[iter + 2];
        if slice > last{
            total += 1;
        }
        iter += 1;
        last = slice;
    }
    total
}
