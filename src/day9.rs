
#[derive(Debug)]
struct Basin{
    basin: Vec<(usize, usize)>,
}

impl Basin {
    fn new() -> Basin{
        Basin{
            basin: Vec::new(),
        }
    }

    fn search(&mut self, matrix: &[Vec<usize>], start: (usize, usize)){
        if !self.basin.contains(&start){
            self.basin.push(start);
            let set_i = start.1;
            let set_j = start.0;
            //Go right
            if set_i < matrix[0].len() - 1 && matrix[set_j][set_i + 1] != 9{
                self.search(matrix, (set_j, set_i + 1));
            }
            //Go left
            if set_i > 0 && matrix[set_j][set_i - 1] != 9{
                self.search(matrix, (set_j, set_i - 1));
            }
            //Go up
            if set_j > 0 && matrix[set_j - 1][set_i] != 9{
                self.search(matrix, (set_j - 1, set_i));
            }
            //Go down
            if set_j < matrix.len() - 1 && matrix[set_j + 1][set_i] != 9{
                self.search(matrix, (set_j + 1, set_i));
            }
        }
    }
}

fn find_low_points(matrix: &[Vec<usize>]) -> Vec<(usize,usize)>{
    let y_len = matrix.len() - 1;
    let x_len = matrix[0].len() - 1;
    let mut total = Vec::new();
    for j in 0..(y_len + 1){
        for i in 0..(x_len + 1){
            let current = matrix[j][i];
            let left = if i == 0 {99} else {matrix[j][i - 1]};
            let right = if i == x_len {99} else {matrix[j][i + 1]};
            let up = if j == y_len {99} else {matrix[j + 1][i]};
            let down = if j == 0 {99} else {matrix[j - 1][i]};
            if current < left && current < right && current < up && current < down{
                total.push((j,i));
            }
        }
    }
    total
}


pub fn solve_part_1() -> usize{
    let data = std::fs::read_to_string("./inputs/day9.txt").unwrap();
    let mut matrix: Vec<Vec<usize>> = Vec::new();
    let rows: Vec<&str> = data.lines().collect();
    for row in rows{
        let v: Vec<usize> = row.chars().map(|s| String::from(s).parse().unwrap()).collect();
        matrix.push(v);
    }
    let mut total = 0;
    for low in find_low_points(&matrix){
        total += matrix[low.0][low.1] + 1;
    }
    total
}

pub fn solve_part_2() -> usize{
    let data = std::fs::read_to_string("./inputs/day9.txt").unwrap();
    let mut matrix: Vec<Vec<usize>> = Vec::new();
    let mut basin_list: Vec<usize> = Vec::new();
    let rows: Vec<&str> = data.lines().collect();
    for row in rows{
        let v: Vec<usize> = row.chars().map(|s| String::from(s).parse().unwrap()).collect();
        matrix.push(v);
    }
    for point in find_low_points(&matrix){
        let mut basin = Basin::new();
        basin.search(&matrix, point);
        basin_list.push(basin.basin.len());
    }
    basin_list.sort_unstable();
    basin_list.reverse();
    basin_list[0..3].iter().product()
}
