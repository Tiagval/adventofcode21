use std::fs;
use std::io::{self, BufRead};
use std::path::Path;

//https://doc.rust-lang.org/rust-by-example/std_misc/file/read_lines.html
fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<fs::File>>>
where P: AsRef<Path>, {
    let file = fs::File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}

pub enum Momentum {
    Forward { x: i64 },
    Down { x: i64 }
}

fn get_data(filename: &str) -> Result<Vec<Momentum>, ()>{
    let mut v = Vec::new();

    if let Ok(lines) = read_lines(filename) {
        for line in lines{
            let l = line.unwrap();
            let split = l.split(' ').collect::<Vec<&str>>();
            let mut mom = Momentum::Forward{x : -1 };
            let dir = &split[0];
            let len: i64 = split[1].parse().unwrap();

            if dir == &"forward"{mom = Momentum::Forward{x : len};}
            if dir == &"down" {mom = Momentum::Down{x : len};}
            if dir == &"up"  {mom = Momentum::Down{x : -len};}
            v.push(mom);
        }
    }
    Ok(v)
}

pub fn solve_part_1()->i64{
    let filename = "./inputs/day2.txt";
    let data = get_data(filename);
    let mut horizontal = 0;
    let mut vertical = 0;
    for momentum in data.unwrap(){
        match momentum{
            Momentum::Forward{x} => horizontal += x,
            Momentum::Down{x} => vertical += x,
        }
    }
    horizontal*vertical
}

pub fn solve_part_2()->i64{
    let filename = "./inputs/day2.txt";
    let data = get_data(filename);
    let mut horizontal = 0;
    let mut vertical = 0;
    let mut aim = 0;

    for momentum in data.unwrap(){
        match momentum {
            Momentum::Down{x} => aim += x,
            Momentum::Forward{x} => {horizontal += x; vertical += aim*x},
        }
    }
    horizontal*vertical
}