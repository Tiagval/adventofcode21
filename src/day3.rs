use std::fs;

fn convert_to_decimal(gamma: &[u32]) -> i64{
    let mut reversed = gamma.to_owned();
    reversed.reverse();
    let mut total: i64 = 0;
    for (index, bit) in reversed.iter().enumerate(){
        let base: i64 = 2;
        total += *bit as i64*base.pow(index as u32);
    }
    total
}

fn get_size(data: &str) -> usize{
    let mut i = 0;
    let mut size = 0;
    for line in data.lines(){
        while i < 1 {
            size = line.len();
            i += 1;
        }
    }
    size
}

fn get_counter(data: &str, size: usize) -> (Vec<u16>, f64){
    let lines = data.lines();
    let mut counter = vec![0; size];
    let mut no_lines:f64 = 0.0;    
    for line in lines {
        for (index, bit) in line.chars().enumerate(){
            if bit == '1' {counter[index] += 1}
        }
        no_lines += 1.0;
    }
    (counter, no_lines)
}

fn get_counter_index(data: &str, target_index : usize, target_bit: char) -> char{
    let lines = data.lines();
    let mut counter: f64 = 0.0;
    let mut no_lines:f64 = 0.0;    
    for line in lines {
        for (index, bit) in line.chars().enumerate(){
            if (target_index == index) && (bit == target_bit) {counter += 1.0}
        }
        no_lines += 1.0;
    }
    let remainder = no_lines - counter;
    if counter > remainder {
        '1'
    }
    else if counter == remainder {target_bit}
    else {'0'}
}

fn get_power(filename: &str) -> i64 {
    let data = fs::read_to_string(filename).unwrap();
    let temp_data = data.clone();
    let size = get_size(&data);

    let (counter, no_lines) = get_counter(&temp_data, size);

    let mut gamma = vec![0; size];
    let mut epsilon = vec![0; size];

    for (index, count) in counter.iter().enumerate(){
        if *count as f64 > (no_lines/2.0).round(){
            gamma[index] = 1;
        }
        else{
            epsilon[index] = 1;
        }
    }
    let decimal_gamma = convert_to_decimal(&gamma);
    let decimal_epsilon = convert_to_decimal(&epsilon);
    decimal_epsilon*decimal_gamma
}

fn filter_events(data: &str, target_bit: char, target_index: usize) -> (String, usize){
    let mut filtered_data = String::new();
    let lines = data.lines();
    let mut no_lines: usize = 0;

    for line in lines{
        for (index, bit) in line.chars().enumerate(){
            if index != target_index {continue;}
            if bit == target_bit{
                filtered_data.push_str(line);
                filtered_data.push('\n');
                no_lines += 1;
            }
        }
    }
    (filtered_data, no_lines)
}

fn filter_loop(data: &str, size: usize, label_bit: char) -> String{
    let mut tmp_data = String::from(data);
    let mut tmp_remaining: usize;
    for i in 0..size{
        let target_bit = get_counter_index(&tmp_data, i, label_bit);
        let (filtered_data, no_remaining) = filter_events(&tmp_data, target_bit, i);
        tmp_data = filtered_data;
        tmp_remaining = no_remaining;
        if tmp_remaining == 1 {return tmp_data;}
    }
    tmp_data
}

fn get_support(filename: &str) -> i64{
    let data = fs::read_to_string(filename).unwrap();
    let size = get_size(&data);

    let mut temp_oxygen = filter_loop(&data, size, '1');
    let mut temp_co2 = filter_loop(&data, size, '0');
    
    let mut oxygen = vec![2; size];
    let mut co2 = vec![2; size];
    //Remove trailing '/n'
    temp_oxygen.pop();
    temp_co2.pop();
    if (temp_oxygen.len() != oxygen.len()) || (temp_co2.len() != co2.len()){
        println!("tmp oxy: {}", temp_oxygen.len());
        println!("oxy: {}", oxygen.len());
        println!("tmp co2: {}", temp_co2.len());
        println!("co2: {}", co2.len());
    }
    for (i, c) in temp_oxygen.chars().enumerate(){
        const RADIX: u32 = 10;
        oxygen[i] = c.to_digit(RADIX).unwrap();
    }
    for (i, c) in temp_co2.chars().enumerate(){
        const RADIX: u32 = 10;
        co2[i] = c.to_digit(RADIX).unwrap();
    }

    let decimal_oxygen = convert_to_decimal(&oxygen);
    let decimal_co2 = convert_to_decimal(&co2);
    decimal_oxygen*decimal_co2
}

pub fn solve_part_1() -> i64 {
    
    get_power("./inputs/day3.txt")
}

pub fn solve_part_2() -> i64{
    
    get_support("./inputs/day3.txt")
}
