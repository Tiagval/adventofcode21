#[derive(std::hash::Hash,Debug)]
#[derive(std::cmp::Eq)]
struct Node{
    id: String,
    visited: bool,
    is_big: bool
}

impl Node{
    fn new(id: String) -> Node{
        Node{
            id: id,
            is_big: false,
            visited: false,
        }
    }
}

impl Clone for Node{
    fn clone(&self) -> Node{
        Node{
            id: self.id.clone(),
            is_big: self.is_big,
            visited: self.visited,
        }
    }
}

impl PartialEq for Node{
    fn eq(&self, other: &Self) -> bool{
        self.id == other.id
    }
}

#[derive(Debug)]
struct Graph{
    edges: std::collections::HashMap<Node, Vec<Node>>
}

impl Graph{
    fn new() -> Graph{
        Graph{
        edges: std::collections::HashMap::new()
        }
    }
}

fn get_graph() -> Graph{
    let mut graph = Graph::new();
    let data = std::fs::read_to_string("./inputs/test_day12.txt").unwrap();
    for line in data.lines(){
        let node_name: Vec<&str> = line.split('-').collect();
        let mut parent = Node::new(node_name[0].to_string());
        parent.is_big = node_name[0].chars().nth(0).unwrap().is_ascii_uppercase();
        let mut children = Node::new(node_name[1].to_string());
        children.is_big = node_name[1].chars().nth(0).unwrap().is_ascii_uppercase();
        if !graph.edges.contains_key(&parent){
            graph.edges.insert(parent, Vec::from([children]));
        }
        else{
            graph.edges.get_mut(&parent).unwrap().push(children);
        }
    }
    for node in graph.edges.keys(){
        println!("{:?}, {:?}", node, graph.edges[node]);
    }
    graph
}

pub fn solve_part_1() -> usize{
    get_graph();
    1
}

pub fn solve_part_2() -> usize{
    1
}
