fn get_data()->Vec<Vec<usize>>{
    let data = std::fs::read_to_string("./inputs/day11.txt").unwrap();
    let mut matrix: Vec<Vec<usize>> = Vec::new();
    for line in data.lines(){
        let v: Vec<usize> = line.chars().map(|c| String::from(c).parse().unwrap()).collect();
        matrix.push(v);
    }
    matrix
}
fn propagate_flash(mut matrix: Vec<Vec<usize>>, pos: (usize, usize), mut flashs: Vec<(usize, usize)>) -> (Vec<Vec<usize>>, Vec<(usize, usize)>){
    matrix[pos.0][pos.1] = 0;
    flashs.push((pos.0, pos.1));
    if pos.1 > 0 && !flashs.contains(&(pos.0, pos.1 -1)){
        let left = matrix[pos.0][pos.1 - 1];
        if left < 9 {
            matrix[pos.0][pos.1 - 1] += 1;
        }
        else{
            let out = propagate_flash(matrix, (pos.0, pos.1 - 1), flashs);
            matrix = out.0;
            flashs = out.1;

        }
    }
    if pos.1 < matrix[0].len() - 1 && !flashs.contains(&(pos.0,pos.1 + 1)){
        let right = matrix[pos.0][pos.1 + 1];
        if right < 9{
            matrix[pos.0][pos.1 + 1] += 1;
        }
        else{
            let out = propagate_flash(matrix,(pos.0, pos.1 + 1), flashs);
            matrix = out.0;
            flashs = out.1;
        }
    }
    if pos.0 > 0 && !flashs.contains(&(pos.0 - 1,pos.1)){
        let up = matrix[pos.0 - 1][pos.1];
        if up < 9{
            matrix[pos.0 - 1][pos.1] += 1;
        }
        else{
            let out = propagate_flash(matrix, (pos.0 - 1, pos.1), flashs);
            matrix = out.0;
            flashs = out.1;
        }
    }
    if pos.0 < matrix.len() - 1 && !flashs.contains(&(pos.0 + 1, pos.1)){
        let down = matrix[pos.0 + 1][pos.1];
        if down < 9{
            matrix[pos.0 + 1][pos.1] += 1;
        }
        else{
            let out = propagate_flash(matrix, (pos.0 + 1, pos.1), flashs);
            matrix = out.0;
            flashs = out.1;
        }
    }
    if pos.0 > 0 && pos.1 > 0 && !flashs.contains(&(pos.0 - 1, pos.1 - 1)){
        let diag_ul = matrix[pos.0 - 1][pos.1 - 1]; //Up left
        if diag_ul < 9{
            matrix[pos.0 - 1][pos.1 - 1] += 1;
        }
        else{
            let out = propagate_flash(matrix, (pos.0 - 1, pos.1 - 1), flashs);
            matrix = out.0;
            flashs = out.1;
        }
    }
    if pos.0 < matrix.len() - 1 && pos.1 > 0 && !flashs.contains(&(pos.0 + 1, pos.1 - 1)){
        let diag_dl = matrix[pos.0 + 1][pos.1 - 1]; //Down left
        if diag_dl < 9{
            matrix[pos.0 + 1][pos.1 - 1] += 1;
        }
        else{
            let out = propagate_flash(matrix, (pos.0 + 1, pos.1 -1), flashs);
            matrix = out.0;
            flashs = out.1;
        }
    }
    if pos.0 > 0 && pos.1 < matrix[0].len() - 1 && !flashs.contains(&(pos.0 - 1, pos.1 + 1)){
        let diag_ru = matrix[pos.0 - 1][pos.1 + 1]; //Up right
        if diag_ru < 9{
            matrix[pos.0 - 1][pos.1 + 1] += 1;
        }
        else{
            let out = propagate_flash(matrix, (pos.0 - 1, pos.1 + 1), flashs);
            matrix = out.0;
            flashs = out.1;
        }
    }
    if pos.0 < matrix.len() - 1 && pos.1 < matrix[0].len() - 1 && !flashs.contains(&(pos.0 + 1, pos.1 + 1)){
        let diag_rd = matrix[pos.0 + 1][pos.1 + 1]; //Down right
        if diag_rd < 9{
            matrix[pos.0 + 1][pos.1 + 1] += 1;
        }
        else{
            let out = propagate_flash(matrix, (pos.0 + 1, pos.1 + 1), flashs);
            matrix = out.0;
            flashs = out.1;
        }
    }
    (matrix.to_vec(), flashs)
}

fn do_step(mut matrix: Vec<Vec<usize>>) -> (Vec<Vec<usize>>, usize){
    let x_len: usize = matrix[0].len();
    let y_len: usize = matrix.len();
    let mut n_flashs = 0;
    let mut flashs = Vec::new();
    for j in 0..(y_len){
        for i in 0..(x_len){
            if !flashs.contains(&(j,i)) {matrix[j][i] += 1;}
            if matrix[j][i] > 9{
                let out = propagate_flash(matrix.to_vec(), (j, i), flashs.to_vec());
                matrix = out.0;
                flashs = out.1;
            }
        }
    }
    n_flashs += flashs.len();
    (matrix.to_vec(), n_flashs)
}

pub fn solve_part_1() -> usize{
    let mut matrix = get_data();
    let mut n_flashs = 0;
    for _ in 0..100{
        let out = do_step(matrix);
        matrix = out.0;
        n_flashs += out.1;
    }
    n_flashs
}

pub fn solve_part_2() -> usize{
    let mut matrix = get_data();
    let mut i = 0;
    loop{
        i += 1;
        let out = do_step(matrix);
        matrix = out.0;
        if out.1 == matrix.len()*matrix[0].len(){
            return i;
        };
    }
}
